﻿using OutlookWebAddInAPI.Models;
using OutlookWebAddInAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OutlookWebAddInAPI.Controllers
{
    public class ThumbnailController : ApiController
    {
        Youtube yt = new Youtube();

        [Route("thumbnail/{videoId}")]
        public Thumbnail Get(string videoId)
        {
            return yt.GetVideoInfo(videoId);
        }
    }
}
