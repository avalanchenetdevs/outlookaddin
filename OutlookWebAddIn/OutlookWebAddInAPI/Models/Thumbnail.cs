﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OutlookWebAddInAPI.Models
{
    public class Thumbnail
    {
        public string VideoId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }

        public Thumbnail(string videoId)
        {
            VideoId = videoId;
        }
    }
}