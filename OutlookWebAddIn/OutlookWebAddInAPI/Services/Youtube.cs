﻿using Newtonsoft.Json;
using OutlookWebAddInAPI.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace OutlookWebAddInAPI.Services
{
    public class Youtube
    {
        private const string ApiKey = "AIzaSyC0lNrGLfu5N5Wy4sAFw74MFvTCqjKFJ - o";
        private const string UrlVideoData = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id={0}&key={1}";

        public Thumbnail GetVideoInfo(string videoId)
        {
            var t = new Thumbnail(videoId);
            var url = string.Format(UrlVideoData, videoId, ApiKey);

            using (var webClient = new System.Net.WebClient())
            {
                var json = webClient.DownloadString(url);
                dynamic ytData = JsonConvert.DeserializeObject(json);
                if (ytData == null || ytData.items == null || ytData.items.Count <= 0) return t;
                t.Title = ytData.items[0].snippet.title;

                string thumb = ytData.items[0].snippet.thumbnails.high.url;
                t.Url = CreateVideoThumbnail(thumb, t.Title, t.VideoId);
            }

            return t;
        }

        private static string CreateVideoThumbnail(string originalThumbUrl, string title, string videoId)
        {
            //load both images
            Image mainImage = OpenBitmapUrl(originalThumbUrl);
            var playPath = HostingEnvironment.ApplicationPhysicalPath + "/App_Data/play.png";
            var imposeImage = Image.FromFile(playPath);

            //create graphics from main image
            using (var graphics = Graphics.FromImage(mainImage))
            {
                //draw other image on top of main Image
                int x1 = mainImage.Width / 2 - imposeImage.Width / 2, y1 = mainImage.Height / 2 - imposeImage.Height / 2;
                var position = new Rectangle(x1, y1, imposeImage.Width, imposeImage.Height);
                graphics.DrawImage(imposeImage, position);

                //Draw title
                var textPosition = new Rectangle(10, 10, mainImage.Width-10, 24);
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.DrawString(title, new Font("Arial", 14, FontStyle.Bold|FontStyle.Underline), Brushes.White, textPosition);

                //save new image
                var outPath = "Content/img/" + videoId + ".jpg";
                var location = HostingEnvironment.ApplicationPhysicalPath + outPath;
                mainImage.Save(location);
                return outPath;
            }
        }

        private static Bitmap OpenBitmapUrl(string url)
        {
            var request = System.Net.WebRequest.Create(url);
            var response = request.GetResponse();
            var responseStream = response.GetResponseStream();
            if (responseStream == null)
                throw new ArgumentException("Url", "URL Provided did not return a valid image stream.");
            return new Bitmap(responseStream);
        }
    }
}