﻿/// <reference path="/Scripts/FabricUI/MessageBanner.js" />

(function () {
    "use strict";

    var apiUrl = config.apiUrl;

    var app = angular.module('app', [
        'officeuifabric.core',
        'officeuifabric.components.button',
        'officeuifabric.components.messagebar',
        'officeuifabric.components.dialog',
        'officeuifabric.components.table'
    ]);

    app.controller('MessageComposeCtrl', ['$scope', MessageComposeCtrl]);

    function MessageComposeCtrl($scope) {
        $scope.links = [
            {
                name: 'One',
                url: 'unitspro.com'
            },
            {
                name: 'Two',
                url: 'test.com'
            }
        ];

        $scope.youtubeDialog = {
            show: false,
            processing: false,
            link: '',
            width: '',
            height: '',
            open: function () {
                $scope.youtubeDialog.link = '';
                $scope.youtubeDialog.processing = false;
                $scope.youtubeDialog.show = true;
            },
            close: function () {
                $scope.youtubeDialog.show = false;
            },
            insert: function () {
                $scope.youtubeDialog.processing = true;
                executeYoutubeInsert({ link: $scope.youtubeDialog.link, width: $scope.youtubeDialog.width, height: $scope.youtubeDialog.height })
                .always($scope.youtubeDialog.close);
            }
        };

        $scope.messageBanner = {
            show: false,
            content: '',
            type: 'info',
            open: function(type, message){
                $scope.messageBanner.show = true;
                $scope.messageBanner.content = message;
                $scope.messageBanner.type = type;
            },
            close: function () {
                $scope.messageBanner.show = false;
            }
        }

        var executeYoutubeInsert = function (data) {
            var deferred = $.Deferred();
            getYoutubeHTML(data)
              .done(function (html) {
                  Office.context.mailbox.item.body.setSelectedDataAsync(
                    html,
                    { coercionType: Office.CoercionType.Html },
                    function (asyncResult) {
                        if (asyncResult.status != Office.AsyncResultStatus.Succeeded) {
                            $scope.messageBanner.open("error", "Failed to insert YouTube video");
                        }
                    });
              })
              .fail(function () {
                  $scope.messageBanner.open("error", "YouTube link is invalid");
              })
            .always(deferred.resolve);
            return deferred;
        }
    }

  // The Office initialize function must be run each time a new page is loaded.
  Office.initialize = function (reason) {
      angular.bootstrap();
  };


  String.prototype.replaceAll = function (terms) {
      var target = this;
      for (var term in terms) {
          target = target.replace(new RegExp(term, 'g'), terms[term]);
      }
      return target;
  };

  function getYoutubeHTML(data) {
      var deferred = $.Deferred();
      var regex = /(youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9_-]{11})/i;
      var template = '<a href="{link}" style="text-align: center; display: inline-block;"><img src="{url}" alt="YouTube: {title}"{style} /><br>{title}</a>';
      var match = data.link.match(regex);
      if (match) {
          var id = match[2];
          getYoutubeInfo(id, function (info) {
              if (info) {
                  var style = '';
                  if (data.width || data.height) {
                      style += ' style="';
                      if (data.width) style += 'width:' + data.width + 'px;';
                      if (data.height) style += 'height:' + data.height + 'px;';
                      style += '"';
                  }
                  var html = template.replaceAll({
                      '{link}': data.link,
                      '{url}': apiUrl+info.Url,
                      '{title}': info.Title,
                      '{style}': style
                  });
                  deferred.resolve(html);
                  return;
              }
              deferred.reject();
          });
      } else {
          deferred.reject();
      }
      return deferred;
  }

  function getYoutubeInfo(id, callback) {
      $.ajax({
          contentType: 'application/json',
          url: apiUrl+"thumbnail/" + id,
          dataType: 'json',
          crossDomain: true
      })
      .done(function (data) {
          callback(data);
      })
      .error(function (e) {
          console.log(e)
          callback();
      });
  }
})();