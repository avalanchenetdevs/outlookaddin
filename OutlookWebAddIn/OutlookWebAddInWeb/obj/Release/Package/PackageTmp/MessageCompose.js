﻿/// <reference path="/Scripts/FabricUI/MessageBanner.js" />

(function () {
    "use strict";

    var messageBanner,
    dialogUrl = 'https://' + location.host + '/GetYoutubeLink.html',
    apiUrl = config.apiUrl;


    function dialogClose(dialog, inputs) {
        inputs = inputs || 'input';
        $(dialog).find(inputs).each(function () { $(this).val(''); $(this).blur(); });
        $(dialog).hide();
    }

  // The Office initialize function must be run each time a new page is loaded.
  Office.initialize = function (reason) {
    $.fn.Dialog = function () {
        return this.each(function () {
            var dialog = this;

            /** Have the close buttons close the Dialog. */
            $(dialog).find(".js-DialogAction--close").each(function () {
                $(this).on('click', function(){dialogClose(dialog)});
            });
        });
    };
    $.fn.TextField = function () {

        /** Iterate through each text field provided. */
        return this.each(function () {

            /** Does it have a placeholder? */
            if ($(this).hasClass("ms-TextField--placeholder")) {

                /** Hide the label on click. */
                $(this).on('click', function () {
                    $(this).find('.ms-Label').hide();
                });

                /** Hide the label on focus. */
                $(this).find('.ms-TextField-field').on('focus', function () {
                    $(this).siblings('.ms-Label').hide();
                });

                /** Show the label again when leaving the field. */
                $(this).find('.ms-TextField-field').on('blur', function () {

                    /** Only do this if no text was entered. */
                    if ($(this).val().length === 0) {
                        $(this).siblings('.ms-Label').show();
                    }
                });
            }
        });
    };
    $(document).ready(function () {
      var element = document.querySelector('.ms-MessageBanner');
      messageBanner = new fabric.MessageBanner(element);
      messageBanner.hideBanner();
      initCompose();
    });
  };


  String.prototype.replaceAll = function (terms) {
      var target = this;
      for (var term in terms) {
          target = target.replace(new RegExp(term, 'g'), terms[term]);
      }
      return target;
  };

  function initCompose() {
      if ($.fn.Dialog) {
          $('.ms-Dialog').Dialog();
      }
      if ($.fn.TextField) {
          $('.ms-TextField').TextField();
      }
      if ($.fn.Spinner) {
          $('.ms-Spinner').Spinner();
      }
      // Vanilla JS Components
      if (typeof fabric === "object") {
          if ('Spinner' in fabric) {
              var element = document.querySelector('.ms-Spinner');
              var component = new fabric['Spinner'](element);
          }
      }
      $('#insertYouTube').click(insertYouTube);
      $('#youtubeInsert').click(function () {
          var link = $('#youtubeLink').val();
          $('#youtubeDialog').find('button').attr('disabled',true);
          $('#youtubeDialog').find('#spinner').show();
          $('#youtubeDialog').find('#controls').hide();
          executeYoutubeInsert(link, function () {
              $('#youtubeDialog').find('button').removeAttr('disabled');
              $('#youtubeDialog').find('#spinner').hide();
              $('#youtubeDialog').find('#controls').show();
              dialogClose($('#youtubeDialog'), '#youtubeLink');
          });
      });
  }

  function executeYoutubeInsert(link, callback) {
      getYoutubeHTML(link, function (res) {
          if (!res.ok) {
              showNotification("Error", "YouTube link is not valid");
              if (callback) callback();
              return;
          }
          Office.context.mailbox.item.body.setSelectedDataAsync(
          res.html,
          { coercionType: Office.CoercionType.Html },
          function (asyncResult) {
              if (asyncResult.status != Office.AsyncResultStatus.Succeeded) {
                  showNotification("Error", "Failed to insert YouTube video");
              }
              if (callback) callback();
          });
      });
  }

  function insertYouTube() {
      buildYouTubeHTML(executeYoutubeInsert);
  }

  function buildYouTubeHTML(callback) {
      if (Office.context.requirements.isSetSupported('DialogAPI', '1.1')) {
          youtubeDialogOpen(function (success, link) {
              if (success) {
                  callback(link);
              }
          })
      } else {
          $('#youtubeDialog').show();
      }
  }

  function youtubeDialogOpen(callback) {
      Office.context.ui.displayDialogAsync("GetYoutubeLink.html",
          { height: 50, width: 50 }, function (asyncResult) {
              if (asyncResult.status == "failed") {
                  showNotification(asyncResult.error.message);
                  callback(false, '');
              }
              else {
                  dialog = asyncResult.value;
                  /*Messages are sent by developers programatically from the dialog using office.context.ui.messageParent(...)*/
                  dialog.addEventHandler(Microsoft.Office.WebExtension.EventType.DialogMessageReceived, messageHandler);

                  /*Events are sent by the platform in response to user actions or errors. For example, the dialog is closed via the 'x' button*/
                  dialog.addEventHandler(Microsoft.Office.WebExtension.EventType.DialogEventReceived, eventHandler);
                  callback(true, 'Test');
              }
          });
  }

  function getYoutubeHTML(link, callback) {
      var res = { ok: false };
      var regex = /(youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9_-]{11})/i;
      var template = '<a href="{link}" style="text-align: center; display: inline-block;"><img src="{url}" alt="YouTube: {title}"{style} /><br>{title}</a>';
      var match = link.match(regex);
      console.log(match)
      if (match) {
          var id = match[2];
          getYoutubeInfo(id, function (info) {
              if (info) {
                  res.ok = true;
                  var style = '', width = $('#youtubeDialog').find('#youtubeWidth').first().val(), height = $('#youtubeDialog').find('#youtubeHeight').first().val();
                  if (width || height) {
                      style += ' style="';
                      if (width) style += 'width:' + width + 'px;';
                      if (height) style += 'height:' + height + 'px;';
                      style += '"';
                  }
                  res.html = template.replaceAll({
                      '{link}': link,
                      '{url}': info.Url,
                      '{title}': info.Title,
                      '{style}': style
                  });
              }
              callback(res);
          });
      } else {
          callback(res);
      }
  }

  function getYoutubeInfo(id, callback) {
      $.ajax({
          contentType: 'application/json',
          url: apiUrl+"thumbnail/" + id,
          dataType: 'json',
          crossDomain: true
      })
      .done(function (data) {
          callback(data);
      })
      .error(function (e) {
          console.log(e)
          callback();
      });
  }

  function messageHandler(arg) {
      showNotification(arg.message);
      dialog.close();
  }

  // Helper function for displaying notifications
  function showNotification(header, content) {
    $("#notificationHeader").text(header);
    $("#notificationBody").text(content);
    messageBanner.showBanner();
    messageBanner.toggleExpansion();
  }
})();